# Hefestos v1.2

O programa Hefestos é um versionador de banco de dados proprietários da Neoprospcta. Atualmente o programa conta com uma command line interface implementada para três funções distintas: add, check e delete.

## Exemplos

### Adicionando novas entradas ao banco
``` 
python src/cli.py add -f database.fasta -t DATABASE_TYPE -i id_list.tsv
``` 

MANDATORY ARGUMENTS
  -f FILE, --file FILE  
			Database file. Ex: neorefITS_v2.fasta

  -t DATABASE_TYPE, --database_type DATABASE_TYPE
                        Type of target database. Implemented for ITS and 16S
                        databases. Ex. ITS

OPTIONAL ARGUMENTS
  -i INPUT_SEQUENCES, --input_sequences INPUT_SEQUENCES
                        Input sequence ids to add. Defaults to id_list.tsv.

  -d DOWNLOAD_DUMPFILE, --download_dumpfile DOWNLOAD_DUMPFILE
                        Download dump files from NCBI database. Default is
                        True.

  -p DUMPFILE, --dumpfile DUMPFILE
                        Local dump files from NCBI database. Default is .

  -v VERSIONATE, --versionate VERSIONATE
                        Versionate database file. Default is True.

