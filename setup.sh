#!/bin/sh

#Download and configure Blast bin files
wget https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.6.0/ncbi-blast-2.6.0+-x64-linux.tar.gz
tar -zxvf ncbi-blast-2.6.0+-x64-linux.tar.gz
rm ncbi-blast-2.6.0+-x64-linux.tar.gz
mkdir tools
mv ncbi-blast-2.6.0+/bin/* tools/.
rm -rf ncbi-blast-2.6.0+
