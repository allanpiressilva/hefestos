## Download
```
git clone https://bitbucket.org/allanpiressilva/hefestos
```
## Installation
```
python -m venv ~/.venv/hefestos
source ~/.venv/hefestos/bin/activate
(hefestos) pip install --no-cache-dir -r requirements.txt
(hefestos) bash setup.sh
```
---

## Docker

Building by using Dockerfile
```
bash setup.sh
docker build -t hefestos:v1.2 .
```

Running hefestos using docker
```
docker container run --rm -v {local_dir}:/app hefestos:v1.2 <positional arguments> <options>
```

---

## Usage
```
(hefestos) python src/cli.py <positional argument> <options>
```
Hefestos implemented actions:
Function|Description
--------|-------------------------------------------------------------
delete|Delete sequence(s) from database
add|Add sequence(s) to database
check|Check sequence(s) in database

Use -h flag for help. Example:
```
(hefestos) python src/cli.py add -h
```
