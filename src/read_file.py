#!/usr/bin/env python3
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict, List

import pandas as pd
from Bio import SeqIO


class Reader:
    def __init__(self, file_name: str, file_format: str, strategy: Strategy):
        self.file_name = file_name
        self._strategy = strategy
        self.file_format = file_format

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def open_file_function(self) -> List:
        data = self._strategy.open_file(self.file_name, self.file_format)
        return data


class OpenFile(ABC):
    @abstractmethod
    def open_file(self, filename: str, file_format: str):
        ...


class OpenITSRefSeqFile(OpenFile):
    def open_file(
        self, filename: str, file_format: str
    ) -> Dict[str : List[Any]]:
        self.file_format = file_format
        self.file_name = filename
        seq_dict = {
            'seq_id': [],
            'ncbi_id': [],
            'taxid': [],
            'species': [],
            'sequence': [],
        }
        try:
            for record in SeqIO.parse(
                f'{self.file_name}.{self.file_format}', 'fasta'
            ):
                seq_dict['seq_id'].append(
                    record.description.split(';')[0].split(' ')[-4]
                    + '-'
                    + record.description.split(';')[0].split(' ')[-3]
                )
                seq_dict['ncbi_id'].append(
                    record.id.replace('_', '').split('.')[0]
                )
                seq_dict['taxid'].append('')
                species = (
                    record.description.split(' ')[1]
                    + ' '
                    + record.description.split(' ')[2]
                )
                seq_dict['species'].append(species)
                seq_dict['sequence'].append(record.seq)
            seq_dict_df = pd.DataFrame(
                seq_dict,
                columns=['seq_id', 'ncbi_id', 'taxid', 'species', 'sequence'],
            )
            return seq_dict_df
        except FileNotFoundError:
            raise RuntimeError(
                f'File {self.file_name}.{self.file_format} not founded!'
            )
            return seq_dict


class OpenGenericFile(OpenFile):
    def open_file(self, filename: str, file_format: str) -> List[Any]:
        self.file_format = file_format
        self.file_name = filename
        try:
            with open(f'{self.file_name}.{self.file_format}', 'r') as file:
                file = [line.strip() for line in file]
                return file
        except FileNotFoundError:
            raise RuntimeError(
                f'File {self.file_name}.{self.file_format} not founded!'
            )
            return []


class OpenFastaFile(OpenFile):
    def open_file(
        self, filename: str, file_format: str
    ) -> Dict[str : List[Any]]:
        self.file_format = file_format
        self.file_name = filename

        seq_dict = {
            'index': [],
            'ncbi_id': [],
            'taxid': [],
            'species': [],
            'sequence': [],
        }
        try:
            for record in SeqIO.parse(
                f'{self.file_name}.{self.file_format}', 'fasta'
            ):
                seq_dict['index'].append(record.id)
                seq_dict['ncbi_id'].append(record.description.split('_')[1])
                seq_dict['taxid'].append(
                    record.description.split(' ')[1].split('_')[0]
                )
                seq_dict['species'].append(record.description.split('_')[2])
                seq_dict['sequence'].append(record.seq)
            seq_dict_df = pd.DataFrame(
                seq_dict,
                columns=['index', 'ncbi_id', 'taxid', 'species', 'sequence'],
            )
            return seq_dict_df
        except FileNotFoundError:
            raise RuntimeError(
                f'File {self.file_name}.{self.file_format} not founded!'
            )
            return seq_dict


class OpenMapFile(OpenFile):
    def open_file(self, filename: str, file_format: str) -> List[Any]:
        self.file_format = file_format
        self.file_name = filename

        dictionary = {'neo_id': [], 'taxid': []}

        try:
            with open(f'{self.file_name}.{self.file_format}', 'r') as file:
                for line in file:
                    line = line.rstrip()
                    line = line.split('        ')
                    dictionary['neo_id'].append(line[0])
                    dictionary['taxid'].append(line[1])
            return dictionary
        except FileNotFoundError:
            raise RuntimeError(
                f'File {self.file_name}.{self.file_format} not founded!'
            )
            return []


class OpenNodesDumpFile(OpenFile):
    def open_file(
        self, filename: str, file_format: str
    ) -> Dict[str : List[Any]]:
        self.file_format = file_format
        self.file_name = filename

        dataframe = pd.read_csv(
            f'{self.file_name}.{self.file_format}',
            sep='\\t\|\\t',
            header=0,
            engine='python',
        )

        dataframe.columns = [
            'taxid',
            'ptaxid',
            'rank',
            'embl',
            'divid',
            'inhDF',
            'gencode',
            'inhGC',
            'mitochondrial',
            'inhMGC',
            'genbank',
            'hiddST',
            'comments',
        ]

        rank_data = dataframe[['taxid', 'rank']]
        del dataframe
        rank_data = rank_data[rank_data['rank'] == 'species']
        return rank_data


class OpenNamesDumpFile(OpenFile):
    def open_file(
        self, filename: str, file_format: str
    ) -> Dict[str : List[Any]]:
        self.file_format = file_format
        self.file_name = filename

        dataframe = pd.read_csv(
            f'{self.file_name}.{self.file_format}',
            sep='\\t\|\\t',
            header=0,
            engine='python',
        )

        dataframe.columns = ['taxid', 'nametxt', 'unique_name', 'name_class']
        dataframe['name_class'] = dataframe['name_class'].str[:-2]

        dataframe['nametxt'] = dataframe.groupby('taxid')['nametxt'].ffill()
        dataframe = (
            dataframe.groupby('taxid')
            .agg(
                {
                    'nametxt': ', '.join,
                    'unique_name': 'first',
                    'name_class': ', '.join,
                }
            )
            .reset_index()
        )
        names_data = dataframe[['taxid', 'nametxt', 'name_class']]
        del dataframe
        return names_data


def merge_dump_files(
    rank_data: Dict[str : List[Any]], names_data: Dict[str : List[Any]]
) -> Dict[str : List[Any]]:
    full_taxid = pd.merge(
        rank_data, names_data, how='inner', left_on='taxid', right_on='taxid'
    )
    # full_taxid.to_csv('full_taxid_table.csv', index = False)
    return full_taxid
