#!/usr/bin/env python3

from typing import Any, Dict, List

from miscelaneous import *
from run_blast import Blast
from write_file import Writter, WriteFastaFile, WriteMapFile


def delete_entry(
    seq_dict: Dict[str, List[Any]],
    filename: str,
    entry_ids: List[str],
    filter_index: str,
) -> None:
    """
    Search for id sequences and delete them from a given fasta file.
    Args:
        seq_dict (dict): Dictionary containing sequences.
        filename (str): Name of file to open.
        entry_ids (list): List of IDs to be deleted
        filter_index (str): Index to be used to filter.
                            Options: index, taxid, ncbi_id and species.

    Raises:
        RuntimeError: Can not open file.
    """
    try:
        with open(f'deleted_entries.txt', 'w') as deleted_entries:
            for seq_id in entry_ids:
                seq_id = seq_id.rstrip().lstrip()
                deleted_seq = seq_dict[seq_dict[filter_index] == seq_id]
                print(
                    f">{deleted_seq['index']} {deleted_seq['taxid']}_\
                         {deleted_seq['ncbi_id']}_\
                         {deleted_seq['species']}",
                    file=deleted_entries,
                )
                seq_dict = seq_dict[seq_dict[filter_index] != seq_id]
        print(
            f'[{CYAN+get_timestamp()+RESET}] Writing new database fasta file.'
        )
        version = filename.split('_v')[1]
        new_file = Writter(
            seq_dict,
            'fasta',
            filename.split('_v')[0],
            int(version) + 1,
            WriteFastaFile(),
        )
        new_file.strategy = WriteFastaFile()
        new_file.write_file_function()

        print(f'[{CYAN+get_timestamp()+RESET}] Writing new database map file.')
        new_file = Writter(
            seq_dict,
            'map',
            filename.split('_v')[0],
            int(version) + 1,
            WriteMapFile(),
        )
        new_file.strategy = WriteMapFile()
        new_file.write_file_function()

        print(f'[{CYAN+get_timestamp()+RESET}] Generating blast databases.')
        new_version = int(version) + 1
        new_filename = filename.split('_v')[0] + '_v' + str(new_version)
        blast = Blast(
            f'{new_filename}.fasta',
            'fasta',
            'nucl',
            f'{new_filename}.taxid.map',
        )
        blast.run_makeblastdb()

    except:
        raise RuntimeError(f'Delete log file not created!')
