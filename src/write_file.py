#!/usr/bin/env python3
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict, List


class Writter:
    def __init__(
        self,
        file_dict: Dict[str, List[Any]],
        file_format: str,
        file_name: str,
        file_version: float,
        strategy: Strategy,
    ):
        """
        Args:
            file_dict (_type_): Dictionary containing data for the file writting.
            file_format (str): Output file format.
            file_name (str): Output file name.
            file_version (float): Output file name version.
            strategy (Strategy): Strategy class with the implementation.
        """

        self.file_dict = file_dict
        self.file_format = file_format
        self.file_name = file_name
        self.file_version = file_version
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def write_file_function(self) -> List:
        data = self._strategy.write_file(
            self.file_dict, self.file_format, self.file_name, self.file_version
        )
        return data


class WriteFile(ABC):
    """
    Abstract method that will be replaced by strategy implementation.
    """

    @abstractmethod
    def write_file(self, filename: str, file_format: str):
        ...


class WriteMapFile(WriteFile):
    """
    Implementation of the class for map file writting.
    """

    def write_file(
        self,
        file_dict: Dict[str : List[Any]],
        file_format: str,
        file_name: str,
        file_version: float,
    ) -> None:
        self.file_dict = file_dict
        self.file_format = file_format
        self.file_name = file_name
        self.file_version = file_version

        neo_id = 1

        with open(
            f'{self.file_name}_v{self.file_version}.taxid.{self.file_format}',
            'w',
        ) as output_file:
            for taxid in self.file_dict['taxid']:
                printline = 'lcl|{}        {}\n'.format(neo_id, taxid)
                print(f'{printline}', end='', file=output_file)
                neo_id += 1


class WriteFastaFile(WriteFile):
    """
    Implementation of the class for fasta file writting.
    """

    def write_file(
        self,
        file_dict: Dict[str : List[Any]],
        file_format: str,
        file_name: str,
        file_version: float,
    ) -> None:
        self.file_dict = file_dict
        self.file_format = file_format
        self.file_name = file_name
        self.file_version = file_version
        try:
            with open(
                f'{self.file_name}_v{self.file_version}.{self.file_format}',
                'w',
            ) as fasta_output:
                new_index = 1
                for index, row in self.file_dict.iterrows():
                    ncbi_id = row['ncbi_id']
                    taxid = row['taxid']
                    species = row['species']
                    sequence = row['sequence']
                    print(
                        f'>{new_index} {taxid}_{ncbi_id}_{species}\n{sequence}',
                        file=fasta_output,
                    )
                    new_index = new_index + 1
        except FileNotFoundError:
            raise RuntimeError(f'File {file_name} not founded!')
