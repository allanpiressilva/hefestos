#!/usr/bin/env python3
from datetime import datetime
from typing import Any, Dict, List

import pandas as pd

CYAN = '\033[1;36m'
RESET = '\033[0;0m'


def prepare_database_file(
    database_file: Dict[str, List[Any]], df: Dict[str, List[Any]]
):

    seq_dict = {
        'seq_id': [],
        'ncbi_id': [],
        'taxid': [],
        'species': [],
        'sequence': [],
    }

    for index, row in database_file.iterrows():
        seq_dict['seq_id'].append(row['seq_id'].replace('-', '.'))
        seq_id_data = (
            row['seq_id']
            .replace('(', '\(')
            .replace(')', '\)')
            .replace('-', '.')
        )
        seq_dict['ncbi_id'].append(row['ncbi_id'])
        species = row['species']
        seq_dict['species'].append(row['species'])
        seq_dict['sequence'].append(row['sequence'])
        try:
            taxid = df.loc[df['nametxt'].str.contains(seq_id_data, case=False)]
            if not taxid.empty:
                seq_dict['taxid'].append(taxid['taxid'].tolist()[0])
            else:
                taxid = df.loc[df['nametxt'].str.contains(species, case=False)]
                seq_dict['taxid'].append(taxid['taxid'].iloc()[0])
        except:
            seq_dict['taxid'].append('NA')

    seq_dict_df = pd.DataFrame(
        seq_dict, columns=['seq_id', 'ncbi_id', 'taxid', 'species', 'sequence']
    )
    seq_dict_df.to_csv(f'database_file_taxid.csv', index=False)
    return seq_dict_df


def check_database_for_id(
    seq_dict: Dict[str, List], seq_id: List[str], collumn: str
):
    dictionary = seq_dict.to_dict(orient='list')
    matched_id = []
    no_matched_id = []
    selected = {
        'seq_id': [],
        'ncbi_id': [],
        'taxid': [],
        'species': [],
        'sequence': [],
    }
    for sequence in seq_id:
        sequence = sequence.rstrip()
        if sequence in dictionary[collumn]:
            matched_id.append(sequence)
        else:
            no_matched_id.append(sequence)
    selected = seq_dict[seq_dict[collumn].isin(seq_id)]
    if no_matched_id:
        print(
            f'[{CYAN+get_timestamp()+RESET}] The following ids were not founded in database:\n{no_matched_id}'
        )
    return selected.reset_index(drop=True)


def get_timestamp():
    timestamp = datetime.now()
    timestamp = str(timestamp).split('.')[0]
    return timestamp
