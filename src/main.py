#!/usr/bin/env python3
from get_file import *
from make_backup import *
from read_file import *
from run_blast import *
from write_file import *

"""Uso do write_fasta.py"""
# file = 'neorefITS_v2'
# ITS_map = Reader(file, "fasta", OpenGenericFile())
# ITS_map.strategy = OpenFastaFile()
# map_dict = ITS_map.open_file_function()
# filename = 'teste'
# ITS_map = Writter(map_dict, "fasta", filename, 1, WriteFastaFile())
# ITS_map.strategy = WriteFastaFile()
# ITS_map.write_file_function(map_dict)

"""Uso do read_file.py"""
# filename = 'teste'
# ITS_RefSeq = Reader(filename, "fasta", OpenGenericFile())
# ITS_RefSeq.strategy = OpenITSRefSeqFile()
# print(ITS_RefSeq.open_file_function())

"""Uso do get_file.py"""
# filename = ncbi_links['bacteria16S']
# file_path = os.path.abspath('.')+'/database/'
# ITS_RefSeq = Getter(filename[0], file_path, GetTargetedLociFile())
# ITS_RefSeq.strategy = GetTargetedLociFile()
# print(ITS_RefSeq.get_file_function())
