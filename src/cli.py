import argparse
import os
import sys
import textwrap

from delete_entry import *
from get_file import *

# from make_backup import *
from miscelaneous import *
from read_file import *
from run_blast import *
from write_file import *


class CliMethodsException(Exception):
    ...


class CliMethods:
    CLI_VERSION = '1.2.0'

    def __init__(self):
        self.__run()

    def __run(self):
        self.parser = argparse.ArgumentParser(
            prog=' '.join(sys.argv),
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description=textwrap.dedent(
                f"""\
                \33[1m
                ***************************************************************
                *                        HEFESTOS V1.2                        *
                ---------------------------------------------------------------
                *                                                             *
                *              Sequence database update program.              *
                *                                                             *
                ***************************************************************
                \33[0m
                """
            ),
            epilog="Neoprospecta's Bioinformatics Team 2022. All rights reserved.",
            usage='%(prog)s [options]',
            conflict_handler='resolve',
        )
        self.parser.version = self.CLI_VERSION

        self.parser.add_argument('-v', '--version', action='version')
        subparsers = self.parser.add_subparsers(
            help='Hefestos implemented actions', dest='command'
        )

        self.delete_parser = subparsers.add_parser(
            'delete',
            help='Delete sequence(s) from database',
            conflict_handler='resolve',
        )
        self.delete_parser.add_argument(
            '-f',
            '--file',
            help='Database file. Ex: neorefITS_v2.fasta',
            required=True,
            type=str,
        )
        self.delete_parser.add_argument(
            '-ft',
            '--filter',
            help='Filter collumn to be used. Defaults to NCBI_ID.\
                    Options: TAXID, NCBI_ID and SPECIES ',
            required=False,
            default='ncbi_id',
            type=str,
        )
        self.delete_parser.add_argument(
            '-i',
            '--input_sequences',
            help='Input sequences for database deletion. Defaults to id_list.tsv.',
            required=False,
            default='id_list.tsv',
            type=str,
        )
        self.delete_parser.add_argument(
            '-t',
            '--database_type',
            help='Type of target database. Implemented for ITS and 16S databases. Example: ITS',
            required=True,
            type=str,
        )

        self.add_parser = subparsers.add_parser(
            'add',
            help='Add sequence(s) to database',
            conflict_handler='resolve',
        )
        self.add_parser.add_argument(
            '-f',
            '--file',
            help='Database file. Example: neorefITS_v2.fasta',
            required=True,
            type=str,
        )
        self.add_parser.add_argument(
            '-t',
            '--database_type',
            help='Type of target database. Implemented for ITS and 16S databases. Example: ITS',
            required=True,
            type=str,
        )
        self.add_parser.add_argument(
            '-i',
            '--input_sequences',
            help='Input sequences ids to add. Defaults to id_list.tsv.',
            required=False,
            default='id_list.tsv',
            type=str,
        )
        self.add_parser.add_argument(
            '-d',
            '--download_dumpfile',
            help='Download dump files from NCBI database. Default is True.',
            required=False,
            default=True,
            type=bool,
        )
        self.add_parser.add_argument(
            '-p',
            '--dumpfile',
            help='Local dump files from NCBI database. Default is <WORKDIR>.',
            required=False,
            default='.',
            type=str,
        )
        self.add_parser.add_argument(
            '-v',
            '--versionate',
            help='Versionate database file. Default is True.',
            required=False,
            default=True,
            type=bool,
        )

        self.check_parser = subparsers.add_parser(
            'check',
            help='Check sequence(s) in database',
            conflict_handler='resolve',
        )
        self.check_parser.add_argument(
            '-f',
            '--file',
            help='Database file. Ex: neorefITS_v2.fasta',
            required=True,
            type=str,
        )
        self.check_parser.add_argument(
            '-t',
            '--database_type',
            help='Type of target database. Implemented for ITS and 16S databases.',
            required=True,
            type=str,
        )
        self.check_parser.add_argument(
            '-i',
            '--input_sequences',
            help='Input sequences for database deletion. Defaults to id_list.tsv.',
            required=False,
            default='id_list.tsv',
            type=str,
        )
        self.check_parser.add_argument(
            '-ft',
            '--filter',
            help='Filter collumn to be used. Defaults to NCBI_ID.\
                    Options: TAXID, NCBI_ID and SPECIES ',
            required=False,
            default='ncbi_id',
            type=str,
        )

        args = self.parser.parse_args()

        if args.command == 'delete':
            self.__delete_sequences_parser(args)

        elif args.command == 'add':
            self.__add_sequences_parser(args)

        elif args.command == 'check':
            self.__check_sequences_parser(args)

    def __delete_sequences_parser(self, args):
        print(f'[{CYAN+get_timestamp()+RESET}] Delete function was called.')
        print(f'[{CYAN+get_timestamp()+RESET}] Reading IDs file.')
        file = Reader(
            args.input_sequences.split('.')[0],
            args.input_sequences.split('.')[-1],
            OpenGenericFile(),
        )
        file.strategy = OpenGenericFile()
        delete_list = file.open_file_function()
        print(f'[{CYAN+get_timestamp()+RESET}] Reading database file.')
        file = Reader(
            args.file.split('/')[-1].split('.')[0],
            args.file.split('/')[-1].split('.')[-1],
            OpenFastaFile(),
        )
        file.strategy = OpenFastaFile()
        database = file.open_file_function()
        print(
            f'[{CYAN+get_timestamp()+RESET}] Removing IDs from database file.'
        )
        delete_entry(
            database,
            args.file.split('/')[-1].split('.')[0],
            delete_list,
            args.filter.lower(),
        )
        print(f'[{CYAN+get_timestamp()+RESET}] Finished.')

    def __add_sequences_parser(self, args):
        # data_path = args.file.split('/neoref')[0]
        path = os.path.abspath(os.getcwd())
        print(f'[{CYAN+get_timestamp()+RESET}] Add function is called.')
        print(f'[{CYAN+get_timestamp()+RESET}] Reading IDs file.')
        file = Reader(
            args.input_sequences.split('.')[0],
            args.input_sequences.split('.')[-1],
            OpenGenericFile(),
        )
        file.strategy = OpenGenericFile()
        add_list = file.open_file_function()

        print(f'[{CYAN+get_timestamp()+RESET}] Reading database file.')
        file = Reader(
            args.file.split('/')[-1].split('.')[0],
            args.file.split('/')[-1].split('.')[-1],
            OpenFastaFile(),
        )
        file.strategy = OpenFastaFile()
        database = file.open_file_function()

        print(
            f'[{CYAN+get_timestamp()+RESET}] Downloading Target Loci fasta file.'
        )
        target_loci = args.database_type
        target_loci = Getter(target_loci, path, GetTargetedLociFile())
        target_loci.strategy = GetTargetedLociFile()
        target_loci = target_loci.get_file_function()

        print(f'[{CYAN+get_timestamp()+RESET}] Downloading Dump file.')
        dump = Getter('dump', path, GetTaxdumpFile())
        dump.strategy = GetTaxdumpFile()
        dump.get_file_function()

        print(f'[{CYAN+get_timestamp()+RESET}] Preparing Database files.')
        nodes = Reader(f'{path}/nodes', 'dmp', OpenNodesDumpFile())
        nodes.strategy = OpenNodesDumpFile()
        nodes = nodes.open_file_function()
        names = Reader(f'{path}/names', 'dmp', OpenNamesDumpFile())
        names.strategy = OpenNamesDumpFile()
        names = names.open_file_function()
        dmp = merge_dump_files(nodes, names)
        del nodes, names, dump
        refseq = Reader(target_loci, 'fasta', OpenITSRefSeqFile())
        refseq.strategy = OpenITSRefSeqFile()
        refseq = refseq.open_file_function()
        # refseq_dict = prepare_database_file(refseq, dmp)
        del target_loci

        print(
            f'[{CYAN+get_timestamp()+RESET}] Checkin IDs in Refseq database file.'
        )
        sequence_add_list = check_database_for_id(refseq, add_list, 'ncbi_id')
        add_dict = prepare_database_file(sequence_add_list, dmp)

        print(f'[{CYAN+get_timestamp()+RESET}] Adding IDs from database file.')
        new_neoref = pd.concat([database, add_dict], ignore_index=True)

        print(f'[{CYAN+get_timestamp()+RESET}] Writting new database file.')
        new_filename = args.file.split('/')[-1].split('_')[0]

        if args.versionate:
            version = int(args.file.split('_v')[1].split('.')[0]) + 1
        else:
            version = int(args.file.split('_v')[1].split('.')[0])
        new_file = Writter(
            new_neoref, 'fasta', new_filename, int(version), WriteFastaFile()
        )
        new_file.strategy = WriteFastaFile()
        new_file.write_file_function()
        new_file = Writter(
            new_neoref, 'map', new_filename, int(version), WriteMapFile()
        )
        new_file.strategy = WriteMapFile()
        new_file.write_file_function()

        print(f'[{CYAN+get_timestamp()+RESET}] Generating blast databases.')
        new_filename = new_filename + '_v' + str(version)
        blast = Blast(
            f'{new_filename}.fasta',
            'fasta',
            'nucl',
            f'{new_filename}.taxid.map',
        )
        blast.run_makeblastdb()

        print(f'[{CYAN+get_timestamp()+RESET}] Finished.')

    def __check_sequences_parser(self, args):
        # data_path = args.file.split('/neoref')[0]
        print(f'[{CYAN+get_timestamp()+RESET}] Check ID function is called.')
        print(f'[{CYAN+get_timestamp()+RESET}] Reading IDs file.')
        file = Reader(
            args.input_sequences.split('.')[0],
            args.input_sequences.split('.')[-1],
            OpenGenericFile(),
        )
        file.strategy = OpenGenericFile()
        add_list = file.open_file_function()
        print(f'[{CYAN+get_timestamp()+RESET}] Reading database file.')
        file = Reader(
            args.file.split('/')[-1].split('.')[0],
            args.file.split('/')[-1].split('.')[-1],
            OpenFastaFile(),
        )
        file.strategy = OpenFastaFile()
        database = file.open_file_function()
        print(f'[{CYAN+get_timestamp()+RESET}] Checking IDs in database file.')
        sequence_add_list = check_database_for_id(
            database, add_list, 'ncbi_id'
        )
        del sequence_add_list
        print(f'[{CYAN+get_timestamp()+RESET}] Finished.')


cli = CliMethods()
