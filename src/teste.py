#!/usr/bin/env python
import argparse
import sys
import textwrap

from run_blast import *


def main(command_line=None):
    parser = argparse.ArgumentParser(
        prog=' '.join(sys.argv),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(
            f"""\
                \33[1m
                ***************************************************************
                *                        HEFESTOS V1.1                        *
                ---------------------------------------------------------------
                *                                                             *
                *              Sequence database update program.              *
                *                                                             *
                ***************************************************************
                \33[0m
                """
        ),
        epilog="Neoprospecta's Bioinformatics Team 2022. All rights reserved.",
        usage='%(prog)s [options]',
        conflict_handler='resolve',
    )

    subparsers = parser.add_subparsers(
        help='Hefestos implemented actions',
        dest='command',
    )

    add = subparsers.add_parser(
        'add', help='Add sequence(s) to database', conflict_handler='resolve'
    )
    delete = subparsers.add_parser(
        'delete',
        help='Delete sequence(s) from database',
        conflict_handler='resolve',
    )
    check = subparsers.add_parser(
        'check',
        help='Check sequence(s) in database',
        conflict_handler='resolve',
    )

    args = parser.parse_args(command_line)

    if args.command == 'delete':
        delete_function(delete)

    elif args.command == 'add':
        add_function(add)

    elif args.command == 'check':
        check_function(check)


def add_function(add):
    print('Add function is called.')
    add.add_argument(
        '-f',
        '--file',
        help='Complete path to database file. Ex: /home/ubuntu/database.fasta',
        required=True,
        type=str,
    )
    add.add_argument(
        '-t',
        '--database_type',
        help='Type of target database. Implemented for ITS and 16S databases.',
        required=True,
        type=str,
    )
    add.add_argument(
        '-i',
        '--input_sequences',
        help='Input sequences for database deletion. Defaults to delete_list.tsv.',
        required=False,
        default='delete_list.tsv',
        type=str,
    )
    add.add_argument(
        '-d',
        '--download_dumpfile',
        help='Download dump files from NCBI database. Defaults is False.',
        required=False,
        default=False,
        type=bool,
    )
    add.add_argument(
        '-p',
        '--dumpfile',
        help='Download dump files from NCBI database. Defaults is False.',
        required=True,
        default='*.dmp',
        type=bool,
    )


def delete_function(delete):
    print('Delete function is called.')
    delete.add_argument(
        '-f',
        '--file',
        help='Complete path to database file. Ex: /home/ubuntu/database.fasta',
        required=True,
        type=str,
    )
    delete.add_argument(
        '-t',
        '--database_type',
        help='Type of target database. Implemented for ITS and 16S databases.',
        required=True,
        type=str,
    )
    delete.add_argument(
        '-i',
        '--input_sequences',
        help='Input sequences for database deletion. Defaults to delete_list.tsv.',
        required=False,
        default='delete_list.tsv',
        type=str,
    )


def check_function(check):
    print('Check function is called.')


if __name__ == '__main__':
    blast = Blast(
        'neorefITS_v3.fasta', 'fasta', 'nucl', 'neorefITS_v3.taxid.map'
    )
    blast.run_makeblastdb()
