from __future__ import annotations

import os
from abc import ABC, abstractmethod
from collections import namedtuple
from typing import Any, Dict, List

BUCKETS = namedtuple(
    'BUCKETS', ['neoref16s_v1', 'neorefits_v1', 'neorefits_v2']
)

BIOINFO_VALID_BUCKETS_LISTS = BUCKETS(
    'others_bkps/neorefs/neoref16s/v1',
    'others_bkps/neorefs/neorefits/v1',
    'others_bkps/neorefs/neorefits/v2',
)

# @TODO:Criar tratamento para entrada dos dados para backup. Atualmente a função requer o path.
class Saver:
    def __init__(self, file_list: List[Any], path: str, strategy: Strategy):
        self.file_list = file_list
        self.path = path
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def backup_file_function(self) -> List[Any]:
        data = self._strategy.backup_file(self.file_list, self.path)
        return data


class BackupFile(ABC):
    @abstractmethod
    def backup_file(self, file_list: List[Any], path: str):
        ...


class BackupGcloud(BackupFile):
    def backup_file(self, file_list: List[Any], path: str):
        self.file_list = file_list
        self.path = path
        for item in self.file_list:
            cmd = f'gsutil cp {item} gs://{self.path}'
            print(cmd)
            os.system(cmd)


class PrepareBackup:
    def __init__(self, database_name: str, database_version: float):
        self.database_name = database_name
        self.database_version = database_version

    def define_backup_file(self):
        file_list = [
            f'delete.py',
            f'delete.txt',
            f'{self.database_name}_{self.database_version}.fasta',
            f'{self.database_name}_{self.database_version}.fasta.nhr',
            f'{self.database_name}_{self.database_version}.fasta.nin',
            f'{self.database_name}_{self.database_version}.fasta.nog',
            f'{self.database_name}_{self.database_version}.fasta.nsd',
            f'{self.database_name}_{self.database_version}.fasta.nsi',
            f'{self.database_name}_{self.database_version}.fasta.nsq',
            f'{self.database_name}_{self.database_version}.taxid.map',
        ]
        return file_list
