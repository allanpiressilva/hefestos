import os


class Blast:

    # @TODO: Verificar implementação de outras funcionalidades do Blast
    def __init__(
        self, db_fasta: str, input_type: str, db_type: str, taxid_file: str
    ) -> None:
        self.db_fasta = db_fasta   #'neorefITS_v2.fasta'
        self.input_type = input_type   #'fasta'
        self.db_type = db_type   #'nucl'
        self.taxid_file = taxid_file   #'neorefITS_v2.taxid.map'

    def run_makeblastdb(self) -> None:
        cmd = f'makeblastdb -in {self.db_fasta}\
                -input_type fasta\
                -dbtype nucl\
                -taxid_map {self.taxid_file}\
                -parse_seqids'
        try:
            os.system(cmd)
        except Exception as error:
            raise RuntimeError(f'One error was identified: {error}')
        else:
            del self.taxid_file

    def run_blastn(self) -> None:
        ...
