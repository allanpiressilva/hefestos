from __future__ import annotations

import os
from abc import ABC, abstractmethod
from datetime import datetime
from typing import Any, Dict, List

from miscelaneous import *

ncbi_links = {
    '16S': [
        'https://ftp.ncbi.nlm.nih.gov/refseq/TargetedLoci/Bacteria/bacteria.16SrRNA.fna.gz'
    ],
    'ITS': [
        'https://ftp.ncbi.nlm.nih.gov/refseq/TargetedLoci/Fungi/fungi.ITS.fna.gz'
    ],
    'taxdump': ['https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz'],
}


class Getter:
    def __init__(self, link: str, file_path: str, strategy: Strategy):
        if link == 'ITS':
            self.link = ncbi_links['ITS']
        elif link == '16S':
            self.link = ncbi_links['16S']
        elif link == 'dump':
            self.link = ncbi_links['taxdump']

        self.path = file_path
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def get_file_function(self) -> List[Any]:
        data = self._strategy.get_file(self.link, self.path)
        return data


class GetFile(ABC):
    @abstractmethod
    def get_file(self, link: str, path: str) -> None:
        ...


class GetTaxdumpFile(GetFile):
    def get_file(self, link: str, file_path: str) -> None:
        self.link = link
        self.path = file_path

        if os.path.exists(self.path):
            print(
                f'[{CYAN+get_timestamp()+RESET}] Dowloading Dump files into {self.path}.'
            )
        else:
            os.system(f'mkdir {self.path}')
            print(f'[{CYAN+get_timestamp()+RESET}] {self.path} not found!')
            print(
                f'[{CYAN+get_timestamp()+RESET}] Creating folder {self.path}.'
            )
            print(
                f'[{CYAN+get_timestamp()+RESET}] Dowloading files into {self.path}.'
            )

        try:
            cmd = f'wget {self.link[0]} -q'
            filename = self.link[0].split('/')[-1]
            os.system(cmd)
            os.system(f'tar -zxf {filename} -C {self.path}')
            print(f'[{CYAN+get_timestamp()+RESET}] Removing unecessary files.')
            unecessary_files = [
                'delnodes.dmp',
                'gencode.dmp',
                'merged.dmp',
                'division.dmp',
                'citations.dmp',
                'readme.txt',
                'gc.prt',
                'taxdump.tar.gz',
            ]
            for name in unecessary_files:
                os.system(f'rm {self.path}/{name}')
        except ConnectionError as error:
            print(error)


class GetTargetedLociFile(GetFile):
    def get_file(self, link: str, file_path: str) -> str:
        self.link = link
        self.path = file_path
        if os.path.exists(self.path):
            print(
                f'[{CYAN+get_timestamp()+RESET}] Dowloading files into {self.path}.'
            )
        else:
            os.system(f'mkdir {self.path}')
            print(f'[{CYAN+get_timestamp()+RESET}] {self.path} not found!')
            print(
                f'[{CYAN+get_timestamp()+RESET}] Creating folder {self.path}.'
            )
            print(
                f'[{CYAN+get_timestamp()+RESET}] Dowloading files into {self.path}.'
            )
        try:
            cmd = f'wget {self.link[0]} -q'
            filename = self.link[0].split('/')[-1]
            os.system(cmd)
            if self.path != os.path.abspath(os.getcwd()):
                os.system(f'mv {filename} {self.path}')
            output_name = filename.split('.')[0] + '_' + filename.split('.')[1]
            os.system(f'gunzip {self.path}/{filename} -c >{output_name}.fasta')
            os.system(f'rm {filename}')
            return output_name
        except ConnectionError as error:
            print(error)


# filename = ncbi_links['bacteria16S']
# file_path = os.path.abspath('.')+'/database/'
# ITS_RefSeq = Getter(filename[0], file_path, GetTargetedLociFile())
# ITS_RefSeq.strategy = GetTargetedLociFile()
# print(ITS_RefSeq.get_file_function())
