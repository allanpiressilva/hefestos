FROM python:3.8-slim

WORKDIR /app

COPY . .

RUN apt-get update \
&& apt-get upgrade -y \
&& apt-get install wget -y \
&& apt-get install libidn11 -y\
&& python3 -m pip install --upgrade pip \
&& pip install --no-cache-dir -r requirements.txt

ADD ./tools/* /usr/local/bin/

ENTRYPOINT ["python","./src/cli.py"]
